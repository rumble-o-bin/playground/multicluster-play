CLUSTER_NAME="kind-infra-mgmt"
CLUSTER_VERSION="kindest/node:v1.27.11"

.PHONY: help
help:  ## Show help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

start_cluster: ## start kind cluster
	@kind create cluster --name $(CLUSTER_NAME) --config .kind/cluster/kind-config.yaml --image $(CLUSTER_VERSION)
	@sleep 1
	@kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.14.5/config/manifests/metallb-native.yaml
	@sleep 30
	@kubectl apply -k .kind/cluster/

delete_cluster: ## Delete cluster kind
	@kind delete cluster --name $(CLUSTER_NAME)

install_argocd: ## install argocd
	@helm upgrade --install -n argocd --create-namespace argocd 0-bootstrap/argocd/ -f 0-bootstrap/argocd/values.yaml

forward_argocd: ## forward argocd
	@kubectl port-forward -n argocd svc/argocd-server 8080:80

get_secret_argocd: ## get argocd secret
	@kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

start_vcluster: ## start vcluster
	@vcluster create c1 -n c1 --connect=false -f vcluster-bootstrap/c1-values.yaml --expose
	@vcluster create c2 -n c2 --connect=false -f vcluster-bootstrap/c2-values.yaml --expose